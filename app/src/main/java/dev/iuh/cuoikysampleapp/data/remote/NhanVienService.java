package dev.iuh.cuoikysampleapp.data.remote;

import java.util.List;

import dev.iuh.cuoikysampleapp.data.model.NhanVien;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface NhanVienService {
	@GET("{id}")
	Call<NhanVien> get(@Path("id") String id);

	@GET("/")
	Call<List<NhanVien>> getAll();

	@POST("add")
	Call<NhanVien> add(@Body NhanVien nhanVien);

	@PUT("update")
	Call<NhanVien> update(@Path("id") String id, @Body NhanVien nhanVien);

	@DELETE("{id}")
	Call<NhanVien> delete(@Path("id") String id);
}
