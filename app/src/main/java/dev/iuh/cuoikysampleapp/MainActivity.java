package dev.iuh.cuoikysampleapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;
import java.util.List;

import dev.iuh.cuoikysampleapp.data.local.MyDatabase;
import dev.iuh.cuoikysampleapp.data.local.NhanVienDao;
import dev.iuh.cuoikysampleapp.data.model.NhanVien;
import dev.iuh.cuoikysampleapp.data.remote.NhanVienService;
import dev.iuh.cuoikysampleapp.data.remote.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
	private static final String TAG = MainActivity.class.getSimpleName();
	private FirebaseAuth mAuth;
	private NhanVienDao nhanVienDao;
	private NhanVienService nhanVienService;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mAuth = FirebaseAuth.getInstance();

		doLogin();

		doThemNhanVien();
		
		fetchData();
	}

	private void fetchData() {
		nhanVienService.getAll()
				.enqueue(new Callback<List<NhanVien>>() {
					@Override
					public void onResponse(Call<List<NhanVien>> call, Response<List<NhanVien>> response) {
						if (response.isSuccessful()) {
							// update data recyclerview
						} else {
							if (response.errorBody() != null) {
								Log.d(TAG, response.errorBody().toString());
								Toast.makeText(MainActivity.this, "Có lỗi xảy ra! Xin vui lòng " +
												"thử lại.",
										Toast.LENGTH_SHORT).show();
							}
						}
					}

					@Override
					public void onFailure(Call<List<NhanVien>> call, Throwable t) {
						Toast.makeText(MainActivity.this, "Có lỗi xảy ra! Xin vui lòng kiểm tra đường truyền Internet.", Toast.LENGTH_SHORT).show();
						Log.d(TAG, t.getLocalizedMessage());
					}
				});
	}

	private NhanVien nhanVien;
	private void doThemNhanVien() {
		if (nhanVienService == null) {
			nhanVienService =
					RetrofitClientInstance.getInstance().getRetrofit().create(NhanVienService.class );
		}

		nhanVienService.add(nhanVien)
				.enqueue(new Callback<NhanVien>() {
					@Override
					public void onResponse(Call<NhanVien> call, Response<NhanVien> response) {
						if (response.isSuccessful()) {
							new ThemNhanVienLocalDB().execute(nhanVien);
						} else {
							if (response.errorBody() != null) {
								Log.d(TAG, response.errorBody().toString());
								Toast.makeText(MainActivity.this, "Có lỗi xảy ra! Xin vui lòng " +
												"thử lại.",
										Toast.LENGTH_SHORT).show();
							}
						}
					}

					@Override
					public void onFailure(Call<NhanVien> call, Throwable t) {
						Toast.makeText(MainActivity.this, "Có lỗi xảy ra! Xin vui lòng kiểm tra đường truyền Internet.", Toast.LENGTH_SHORT).show();
						Log.d(TAG, t.getLocalizedMessage());
					}
				});

	}

	String email = "chumin99";
	String password = "123456";

	private void doLogin() {
		if (!(email != null && !email.isEmpty())) {
			return;
		}

		if (!(password != null && !password.isEmpty())) {
			return;
		}

		mAuth.signInWithEmailAndPassword(email, password)
				.addOnSuccessListener(authResult -> {
					if (authResult.getUser() != null) {

					}
				})
				.addOnFailureListener(e -> {

				});
	}

	private class ThemNhanVienLocalDB extends AsyncTask<NhanVien, Void, Boolean> {

		@Override
		protected Boolean doInBackground(NhanVien... nhanViens) {
			if (nhanVienDao == null) {
				nhanVienDao = MyDatabase.getDatabase(MainActivity.this).nhanVienDao();
			}

			if (Arrays.stream(nhanViens).findFirst().isPresent()) {
				NhanVien nv = Arrays.stream(nhanViens).findFirst().get();
				return nhanVienDao.themNhanVien(nv) > 0;
			}

			return false;
		}

		@Override
		protected void onPostExecute(Boolean aBoolean) {
			super.onPostExecute(aBoolean);
			if (aBoolean) {
				// thông báo kết quả
			} else {

			}
		}
	}

}