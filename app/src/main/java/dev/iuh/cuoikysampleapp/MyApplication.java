package dev.iuh.cuoikysampleapp;

import android.app.Application;

import androidx.room.Room;

import dev.iuh.cuoikysampleapp.data.local.MyDatabase;

public class MyApplication extends Application {
	MyDatabase myDatabase;

	@Override
	public void onCreate() {
		super.onCreate();
		myDatabase = Room.databaseBuilder(this, MyDatabase.class, MyDatabase.DATABASE_NAME)
				.fallbackToDestructiveMigration()
				.build();
	}

	public MyDatabase getMyDatabase(){
		return myDatabase;
	}
}
